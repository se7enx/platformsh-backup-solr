# Platform.sh Backup Solr

This repository provides a [PHP script](https://gitlab.com/contextualcode/platformsh-backup-solr/blob/master/bin/php/backup_solr.php), which allows to get a Solr backup files in the `app` container.

## Usage
1. Add [bin/php/backup_solr.php](https://gitlab.com/contextualcode/platformsh-backup-solr/blob/master/bin/php/backup_solr.php) to your Platform.sh project. Make sure, there is `solr` relationship, otherwise adjust [`SolrBackup::getEndpoint`](https://gitlab.com/contextualcode/platformsh-backup-solr/blob/master/bin/php/backup_solr.php#L117-138).
2. Add `backups` mount in your Platform.sh project (or you can reuse any existing one).
3. Deploy to Platform.sh.
4. SSH to Platform.sh project and run the script:
    ```bash
    php bin/php/backup_solr.php /app/backups
    ```
5. After the script is complete, you should get backup for all Solr cores in `/app/backups/cores`.