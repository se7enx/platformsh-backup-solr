<?php

class SolrBackup {

    const DIR_CORES = 'cores';

    private $tempPath = '/tmp';
    private $endpoint = array();

    public function __construct($tempPath)
    {
        if (empty($tempPath) === false && file_exists($tempPath)) {
            $this->tempPath = realpath($tempPath);
        }

        $this->endpoint = $this->getEndpoint();
    }

    public function run()
    {
        if (is_writable($this->tempPath) === false) {
            $this->output('Temporary path "' . $this->tempPath . '" is not writable', true);
        }
        if ($this->endpoint === null) {
            $this->output('Unable to get endpoint', true);
        }

        $cores = $this->getCores();
        if (count($cores) === 0) {
            $this->output('Unable to extract cores', true);
        }

        foreach ($cores as $core) {
            $this->output('Making backup for "' . $core . '" core ...');
            $this->backupCore($core);
        }
    }

    private function getCores() {
        $response = $this->requestApi(null, 'admin/cores');
        return array_keys($response['status']);
    }

    private function backupCore($core) {
        $dir = $this->getCoreDirectory($core);
        if (is_writable($dir) === false) {
            $this->output('Unable to create "' . $dir . '" directory');
            return;
        }

        $this->requestApi($core, 'replication?location=conf&command=backup&numberToKeep=1');
        $secondsToWait = 600;
        while ($secondsToWait > 0) {
            $secondsToWait--;
            sleep(1);

            if ($this->getBackupStatus($core) === 'success') {
                break;
            }
        };
        if ($secondsToWait === 0) {
            $this->output('Unable to fetch backup status');
        }

        $this->downloadBackupFiles($core, $dir);
    }

    private function getCoreDirectory($core) {
        $dir = $this->tempPath . '/' . self::DIR_CORES . '/' . $core;
        if (file_exists($dir)) {
            exec('rm -Rf ' . $dir);
        }
        mkdir($dir, 0755, true);

        return $dir;
    }

    private function getBackupStatus($core) {
        $status = $this->requestApi($core, 'replication?command=details');
        return isset($status['details']['backup'][5]) ? $status['details']['backup'][5] : null;
    }

    private function downloadBackupFiles($core, $dir) {
        $snapshotDir = $this->getSnapshotDir($core);
        if ($snapshotDir === null) {
            $this->output('Unable to extract snapshot directory');
            return;
        }

        $response = $this->requestApi($core, 'admin/file?file=/' . $snapshotDir);
        foreach ($response['files'] as $name => $metadata) {
            $getFileRequest = 'admin/file?file=' . $snapshotDir . '/' . $name;

            // Download file in a chunks
            $input = fopen($this->requestApiUrl($core, $getFileRequest), 'r');
            $output = fopen($dir . '/'. $name, 'w');
            while (!feof($input)) {
                $chunk = fread($input, 1024);
                fwrite($output, $chunk);
            }
            fclose($input);
            fclose($output);
        }
    }

    private function getSnapshotDir($core) {
        $response = $this->requestApi($core, 'admin/file?file=/');
        foreach ($response['files'] as $file => $data) {
            if (strpos($file, 'snapshot.') === 0) {
                return $file;
            }
        }

        return null;
    }

    private function getEndpoint() {
        $relationships = getenv('PLATFORM_RELATIONSHIPS');
        if (!$relationships) {
            return;
        }

        $relationships = json_decode(base64_decode($relationships), true);
        if (isset($relationships['solr']) === false) {
            return;
        }

        foreach ($relationships['solr'] as $endpoint) {
            if ($endpoint['scheme'] !== 'solr') {
                continue;
            }

            return array(
                'host' => $endpoint['host'],
                'port' => $endpoint['port'],
            );
        }
    }

    private function output($string, $isError = false) {
        if ($isError) {
            $string = '[ERROR] ' . $string;
        }
        echo $string . PHP_EOL;

        if ($isError) {
            exit();
        }
    }

    private function requestApi($core, $path) {
        $url = $this->requestApiUrl($core, $path);
        $url .= strpos($url,'?') !== false ? '&' : '?';
        $url .= 'wt=json';

        return json_decode(file_get_contents($url), true);
    }

    private function requestApiUrl($core, $path) {
        $url = 'http://' . $this->endpoint['host'] . ':' .$this->endpoint['port'] . '/solr/';
        if ($core !== null) {
            $url .= $core . '/';
        }
        $url .= $path;

        return $url;
    }
}

$tempPath = isset($argv[1]) ? $argv[1] : null;
$script = new SolrBackup($tempPath);
$script->run();